# README #

Non blocking Message API for delivering sms.

### How do I get set up? ###

* Run /mock/start-mock-server.sh to start up mock server
* Run sms-service/start-service.sh to start up application
* Access /h2-console to view tables and persisted data (Use connection string configured in the application.properties)

### Technologies ###

* Springboot 1.5.2
* H2
* Mock server for integration tests
* Wiremock to mock sms service
* Embedded Tomcat

### Documentation ###
* To view API contract documentation access: http://editor.swagger.io/#!/ and import swagger.yaml in projects root directory.