#!/bin/bash

echo "Downloading mock server"
wget http://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.5.1/wiremock-standalone-2.5.1.jar

echo "Stopping process on port 8989..."
curl http://localhost:8989/__admin/shutdown --silent

echo "Starting mock server..."
java -jar wiremock-standalone-2.5.1.jar --port 8989 --root-dir .