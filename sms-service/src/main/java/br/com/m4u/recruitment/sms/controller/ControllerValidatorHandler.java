package br.com.m4u.recruitment.sms.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;
import br.com.m4u.recruitment.sms.service.ExternalServiceException;

@ControllerAdvice
public class ControllerValidatorHandler extends ResponseEntityExceptionHandler {
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return super.handleExceptionInternal(ex, new ApiError(status, ex), headers, status, request);
	}
	
	@ExceptionHandler(ExpiredMessageException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiError handleExpiredMessageException(ExpiredMessageException ex){
		return new ApiError(HttpStatus.BAD_REQUEST, ex);
	}
	
	@ExceptionHandler({ExternalServiceException.class, Exception.class})
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ApiError handleExpiredException(Exception ex){
		return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex);
	}
}
