package br.com.m4u.recruitment.sms.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.m4u.recruitment.sms.model.Message;
import br.com.m4u.recruitment.sms.processor.MessageProcessor;

@RestController("/message/?")
public class MessageController {
		
	private MessageProcessor processor;
	
	@Autowired
	public MessageController(MessageProcessor processor) {
		this.processor = processor;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public CompletableFuture<ResponseEntity<?>> send(@Validated @RequestBody Message message){
		return processor.process(message);
	}
}
