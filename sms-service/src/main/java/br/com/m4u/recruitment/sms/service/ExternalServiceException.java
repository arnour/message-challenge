package br.com.m4u.recruitment.sms.service;

public class ExternalServiceException extends RuntimeException {
	
	private static final long serialVersionUID = -3091589649103413545L;
	
	public ExternalServiceException(Exception e) {
		super(e);
	}

}
