package br.com.m4u.recruitment.sms;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.m4u.recruitment.sms.service.ExternalServiceProperties;

@Configuration
public class ExternalServiceConfiguration {

	@Bean
    public RestTemplate restTemplate(CloseableHttpClient httpClient, ObjectMapper mapper) {
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
        restTemplate.getMessageConverters().add(0, new MappingJackson2HttpMessageConverter(mapper));
		return restTemplate;
    }
	
	@Bean
	public CloseableHttpClient httpClient(ExternalServiceProperties props){
		
		PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        poolingHttpClientConnectionManager.setMaxTotal(props.getMaxTotal());
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(props.getMaxTotalPerRoute());
        
        SocketConfig socketConfig = SocketConfig.custom().setSoKeepAlive(props.getKeepAlive()).build();
        
        RequestConfig requestConfig = RequestConfig.custom()
			.setConnectionRequestTimeout(props.getConnectionRequestTimeout())
			.setConnectTimeout(props.getConnectionTimeout())
	        .setSocketTimeout(props.getReadTimeout())
        	.build();
		
		return HttpClientBuilder.create()
                .setConnectionManager(poolingHttpClientConnectionManager)
                .setDefaultRequestConfig(requestConfig)
                .setDefaultSocketConfig(socketConfig)
        .build();
	}
}
