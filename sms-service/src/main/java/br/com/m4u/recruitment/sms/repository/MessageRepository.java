package br.com.m4u.recruitment.sms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.m4u.recruitment.sms.model.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

}