package br.com.m4u.recruitment.sms.processor;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.m4u.recruitment.sms.controller.ResponseBuilder;
import br.com.m4u.recruitment.sms.model.Message;
import br.com.m4u.recruitment.sms.repository.MessageRepository;
import br.com.m4u.recruitment.sms.service.MessageService;

@Component
public class MessageProcessor {
	
	private ExecutorService messageProcessorPool;
	private ResponseBuilder responseBuilder;
	private MessageRepository repository;
	private MessageService service;
	
	@Autowired
	public MessageProcessor(ExecutorService messageProcessorPool, ResponseBuilder responseBuilder, MessageRepository repository, MessageService service) {
		this.messageProcessorPool = messageProcessorPool;
		this.responseBuilder = responseBuilder;
		this.repository = repository;
		this.service = service;
	}

	public CompletableFuture<ResponseEntity<?>> process(Message message) {
		message.checkExpiration();
		return CompletableFuture
				.supplyAsync(() -> repository.save(message), messageProcessorPool)
				.thenApply(service::send)
				.thenApply(responseBuilder::created);
	}

}
