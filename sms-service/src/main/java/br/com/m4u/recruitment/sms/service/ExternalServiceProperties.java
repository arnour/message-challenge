package br.com.m4u.recruitment.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ExternalServiceProperties {
	
	@Autowired private Environment environment;

	public String getURL() {
		return environment.getProperty("external.service.url");
	}
	
	public Integer getMaxTotal(){
		return environment.getProperty("external.service.pool.maxTotal", Integer.class);
	}
	
	public Integer getMaxTotalPerRoute(){
		return environment.getProperty("external.service.pool.maxTotalPerRoute", Integer.class);
	}
	
	public Integer getConnectionTimeout(){
		return environment.getProperty("external.service.pool.connectionTimeout", Integer.class);
	}
	
	public Integer getConnectionRequestTimeout(){
		return environment.getProperty("external.service.pool.connectionRequestTimeout", Integer.class);
	}
	
	public Integer getReadTimeout(){
		return environment.getProperty("external.service.pool.readTimeout", Integer.class);
	}
	
	public boolean getKeepAlive(){
		return environment.getProperty("external.service.pool.keepAlive", Boolean.class);
	}

}
