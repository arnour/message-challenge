package br.com.m4u.recruitment.sms;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NioConfiguration {

	@Bean
	public ExecutorService messageProcessorPool(@Value("${pool.size}") int poolSize){
		return Executors.newFixedThreadPool(poolSize);
	}
}
