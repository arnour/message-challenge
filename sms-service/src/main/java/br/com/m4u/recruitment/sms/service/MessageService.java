package br.com.m4u.recruitment.sms.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.m4u.recruitment.sms.model.Message;

@Service
public class MessageService {

	private Logger log = Logger.getLogger(Message.class);
	
	@Autowired private RestTemplate rest;
	@Autowired private ExternalServiceProperties configuration;
	
	public Message send(Message message) throws ExternalServiceException {
		try {
			rest.put(configuration.getURL(), message);
			return message;
		} catch (Exception e) {
			log.error("An error was found when trying to call external sms service for message: " + message.getId(), e);
			throw new ExternalServiceException(e);
		}
	}
}
