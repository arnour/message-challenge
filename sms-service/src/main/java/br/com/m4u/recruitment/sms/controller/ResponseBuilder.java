package br.com.m4u.recruitment.sms.controller;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.m4u.recruitment.sms.model.Message;

@Component
public class ResponseBuilder {

	public ResponseEntity<?> created(Message message){
		return ResponseEntity.created(URI.create("/message/" + message.getId())).build();
	}
}
