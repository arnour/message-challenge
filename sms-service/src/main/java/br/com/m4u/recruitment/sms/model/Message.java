package br.com.m4u.recruitment.sms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;

@Entity
@Table(name = "t_message")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Message {
	
	@Id
	@GeneratedValue
	private Long id;
	@NotNull(message = "message's sender is required")
	@Column(name = "sender")
	private String from;
	@NotNull(message = "message's destination is required")
	@Column(name = "destination")
	private String to;
	@NotNull(message = "message's body is required")
	@Size(min = 1, max = 160, message = "message's body should be between 1 and 160 characters")
	private String body;
	private LocalDateTime expiresAt;
	@JsonIgnore
	private LocalDateTime processedAt = LocalDateTime.now();

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}	
	public LocalDateTime getExpiresAt() {
		return expiresAt;
	}
	public void setExpiresAt(LocalDateTime expiresAt) {
		this.expiresAt = expiresAt;
	}
	public LocalDateTime getProcessedAt() {
		return processedAt;
	}
	public void setProcessedAt(LocalDateTime processedAt) {
		this.processedAt = processedAt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		return true;
	}
	
	public void checkExpiration(){
		if(expiresAt != null && expiresAt.isBefore(LocalDateTime.now())){
			throw new ExpiredMessageException();
		}
	}
}
