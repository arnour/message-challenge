package br.com.m4u.recruitment.sms.controller;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiError {
	private int statusCode;
	private String message;
	
	private Set<String> errors;
	
	public ApiError(HttpStatus status, MethodArgumentNotValidException exception) {
		statusCode = status.value();
		message = exception.getLocalizedMessage();
		if(exception.getBindingResult() != null){
			errors = exception.getBindingResult().getFieldErrors()
					.stream()
					.map(FieldError::getDefaultMessage)
					.collect(Collectors.toSet());
		}
	}

	public ApiError(HttpStatus status, Exception exception) {
		statusCode = status.value();
		message = exception.getLocalizedMessage();
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Set<String> getErrors() {
		return errors;
	}

	public void setErrors(Set<String> errors) {
		this.errors = errors;
	}	
}
