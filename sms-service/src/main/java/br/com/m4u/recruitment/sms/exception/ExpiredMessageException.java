package br.com.m4u.recruitment.sms.exception;

public class ExpiredMessageException extends RuntimeException {
	private static final long serialVersionUID = -1302260388675619791L;

	public ExpiredMessageException() {
		super("message has already expired");
	}
	
	
}
