package br.com.m4u.recruitment.sms.processor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

import br.com.m4u.recruitment.sms.controller.ResponseBuilder;
import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;
import br.com.m4u.recruitment.sms.model.Message;
import br.com.m4u.recruitment.sms.repository.MessageRepository;
import br.com.m4u.recruitment.sms.service.MessageService;

public class MessageProcessorTest {

	
	@Test
	@SuppressWarnings("unchecked")
	public void should_process_message() throws InterruptedException, ExecutionException {
		ExecutorService pool = Executors.newSingleThreadExecutor();
		ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
		MessageRepository repository = mock(MessageRepository.class);
		MessageService service = mock(MessageService.class);
		
		Message saved = mock(Message.class);
		Message message = mock(Message.class);
		Message sent = mock(Message.class);
		
		@SuppressWarnings("rawtypes")
		ResponseEntity response = mock(ResponseEntity.class);
		
		when(repository.save(message)).thenReturn(saved);
		when(service.send(saved)).thenReturn(sent);
		when(responseBuilder.created(sent)).thenReturn(response);
		
		assertThat(new MessageProcessor(pool, responseBuilder, repository, service).process(message).get()).isEqualTo(response);
		
		verify(repository).save(message);
		verify(service).send(saved);
		verify(responseBuilder).created(sent);
		verify(message).checkExpiration();
	}
	
	@Test(expected = ExpiredMessageException.class)
	public void should_not_process_expired_message() throws InterruptedException, ExecutionException {
		ExecutorService pool = Executors.newSingleThreadExecutor();
		ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
		MessageRepository repository = mock(MessageRepository.class);
		MessageService service = mock(MessageService.class);
		
		Message message = mock(Message.class);
		
		doThrow(ExpiredMessageException.class).when(message).checkExpiration();
		
		new MessageProcessor(pool, responseBuilder, repository, service).process(message);
	}

}
