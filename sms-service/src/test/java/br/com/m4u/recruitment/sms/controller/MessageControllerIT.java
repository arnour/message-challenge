package br.com.m4u.recruitment.sms.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import br.com.m4u.recruitment.sms.SmsServiceApplication;
import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;
import br.com.m4u.recruitment.sms.processor.MessageProcessor;

@RunWith(SpringRunner.class)
@WebMvcTest(MessageController.class)
@ContextConfiguration(classes = {SmsServiceApplication.class})
public class MessageControllerIT {
	
	@MockBean private MessageProcessor processor;
	@Autowired private MockMvc mvc;

	@Test
	public void should_send_message_non_blocking() throws Exception {
		
		given(
			processor.process(Mockito.any())
		)
		.willReturn(
			CompletableFuture.supplyAsync(() -> ResponseEntity.created(URI.create("/message/1")).build(), Executors.newSingleThreadExecutor())
		);
		
		MvcResult result = mvc
			.perform(
				post("/message")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content("{\"from\":\"sender\",\"to\":\"destination\",\"body\":\"message\"}")
			)
			.andExpect(request().asyncStarted())
			.andReturn();
		
		mvc.perform(asyncDispatch(result))
			.andExpect(status().isCreated())
			.andExpect(header().string("Location", "/message/1"));
	}
	
	@Test
	public void send_invalid_json_message_with_null_required_fields_should_return_bad_request_and_error_content() throws Exception {
		mvc
			.perform(
				post("/message")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{}")
					.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.statusCode", is(400)))
			.andExpect(jsonPath("$.message", is(notNullValue())))
			.andExpect(jsonPath("$.errors", is(Arrays.asList("message's body is required", "message's sender is required", "message's destination is required"))));
	}
	
	@Test
	public void send_invalid_json_body_message_with_more_than_160_chars_should_return_bad_request_and_error_content() throws Exception {
		this.mvc
			.perform(
				post("/message")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"from\":\"1111111\",\"to\":\"222222\",\"body\":\"Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker\"}")
					.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.statusCode", is(400)))
			.andExpect(jsonPath("$.message", is(notNullValue())))
			.andExpect(jsonPath("$.errors", is(Arrays.asList("message's body should be between 1 and 160 characters"))));
	}

	@Test
	public void send_expired_message_should_return_bad_request_and_error_content() throws Exception {
		
		given(
			processor.process(Mockito.any())
		)
		.willThrow(new ExpiredMessageException());
		
		this.mvc
			.perform(
				post("/message")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"from\":\"1111111\",\"to\":\"222222\",\"body\":\"Lorem Ipsum é simplesmente uma simulaçã\",\"expiresAt\":\"2017-01-01T12:00\"}")
					.accept(MediaType.APPLICATION_JSON)
			)
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.statusCode", is(400)))
			.andExpect(jsonPath("$.message", is(equalTo("message has already expired"))));
	}
}
