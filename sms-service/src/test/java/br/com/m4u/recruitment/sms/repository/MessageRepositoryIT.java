package br.com.m4u.recruitment.sms.repository;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.m4u.recruitment.sms.model.Message;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MessageRepositoryIT {

	@Autowired
	private MessageRepository repository;
	
	@Test
	public void save_message_should_save_and_create_id(){
		
		Message message = new Message();
		message.setFrom("from");
		message.setTo("to");
		message.setBody("body");
		
		Message saved = this.repository.save(message);
		
		assertThat(saved.getId()).isNotNull();
		
		assertThat(this.repository.findOne(saved.getId())).isNotNull();
	}
}