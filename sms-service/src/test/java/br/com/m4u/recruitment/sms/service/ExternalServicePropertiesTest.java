package br.com.m4u.recruitment.sms.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

@RunWith(MockitoJUnitRunner.class)
public class ExternalServicePropertiesTest {
	
	@Mock private Environment environment;
	@InjectMocks private ExternalServiceProperties configuration;

	@Test
	public void should_get_external_service_properties_from_environment_configuration() {
		final String url = "url-configurada";
		
		when(environment.getProperty("external.service.url")).thenReturn(url);
		when(environment.getProperty("external.service.pool.maxTotal", Integer.class)).thenReturn(10);
		when(environment.getProperty("external.service.pool.maxTotalPerRoute", Integer.class)).thenReturn(11);
		when(environment.getProperty("external.service.pool.connectionTimeout", Integer.class)).thenReturn(100);
		when(environment.getProperty("external.service.pool.connectionRequestTimeout", Integer.class)).thenReturn(200);
		when(environment.getProperty("external.service.pool.readTimeout", Integer.class)).thenReturn(300);
		when(environment.getProperty("external.service.pool.keepAlive", Boolean.class)).thenReturn(true);
		
		assertThat(configuration.getURL()).isEqualTo(url);
		assertThat(configuration.getMaxTotal()).isEqualTo(10);
		assertThat(configuration.getMaxTotalPerRoute()).isEqualTo(11);
		assertThat(configuration.getConnectionTimeout()).isEqualTo(100);
		assertThat(configuration.getConnectionRequestTimeout()).isEqualTo(200);
		assertThat(configuration.getReadTimeout()).isEqualTo(300);
		assertThat(configuration.getKeepAlive()).isEqualTo(true);
	}

}
