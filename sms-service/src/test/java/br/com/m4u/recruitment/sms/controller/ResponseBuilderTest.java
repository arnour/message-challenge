package br.com.m4u.recruitment.sms.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

import br.com.m4u.recruitment.sms.model.Message;

public class ResponseBuilderTest {

	@Test
	public void should_create_success_response_for_message() {
		Message message = mock(Message.class);
		
		when(message.getId()).thenReturn(44L);
		
		ResponseEntity<?> response = new ResponseBuilder().created(message);
				
		assertThat(response.getHeaders(), hasEntry(equalTo("Location"), equalTo(Arrays.asList("/message/44"))));
	}

}
