package br.com.m4u.recruitment.sms.service;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.m4u.recruitment.sms.model.Message;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {
	
	@Mock private RestTemplate rest;
	@Mock private ExternalServiceProperties configuration;
	@InjectMocks private MessageService service;

	@Test
	public void should_send_sms_through_external_service() {
		Message message = mock(Message.class);
		
		when(configuration.getURL()).thenReturn("url-servico");
		
		service.send(message);
		
		verify(rest).put("url-servico", message);
		verify(configuration).getURL();
	}
	
	@Test(expected = ExternalServiceException.class)
	public void should_throw_exception_when_calling_external_service() {
		Message message = mock(Message.class);
		
		when(configuration.getURL()).thenReturn("url-servico");
		doThrow(RestClientException.class).when(rest).put("url-servico", message);
		
		service.send(message);
	}

}
