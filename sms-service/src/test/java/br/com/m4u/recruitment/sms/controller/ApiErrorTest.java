package br.com.m4u.recruitment.sms.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;

public class ApiErrorTest {

	@Test
	public void methodArgumentNotValidException_should_generate_an_apiError() {
		MethodArgumentNotValidException error = mock(MethodArgumentNotValidException.class);
		BindingResult bindingResult = mock(BindingResult.class);
		FieldError fieldError = mock(FieldError.class);
		String generalError = "General error description";
		String fieldErrorMessage = "Error found here!";
		
		when(error.getLocalizedMessage()).thenReturn(generalError);
		when(error.getBindingResult()).thenReturn(bindingResult);
		when(bindingResult.getFieldErrors()).thenReturn(Arrays.asList(fieldError));		
		when(fieldError.getDefaultMessage()).thenReturn(fieldErrorMessage);
		
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error);
		
		assertThat(apiError.getMessage()).isEqualTo(generalError);
		assertThat(apiError.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(apiError.getErrors()).contains(fieldErrorMessage);
	}
	
	@Test
	public void methodArgumentNotValidException_without_bindindResult_should_generate_an_apiError() {
		MethodArgumentNotValidException error = mock(MethodArgumentNotValidException.class);
		String generalError = "General error description";
		
		when(error.getLocalizedMessage()).thenReturn(generalError);
		
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error);
		
		assertThat(apiError.getMessage()).isEqualTo(generalError);
		assertThat(apiError.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(apiError.getErrors()).isNull();
	}
	
	@Test
	public void expiredMessageException_should_generate_an_apiError() {
		ExpiredMessageException error = mock(ExpiredMessageException.class);
		String messageError = "General error description";
		
		when(error.getLocalizedMessage()).thenReturn(messageError);
		
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error);
		
		assertThat(apiError.getMessage()).isEqualTo(messageError);
		assertThat(apiError.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(apiError.getErrors()).isNull();
	}

}
