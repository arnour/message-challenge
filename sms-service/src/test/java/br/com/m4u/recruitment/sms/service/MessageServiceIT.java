package br.com.m4u.recruitment.sms.service;

import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.m4u.recruitment.sms.ExternalServiceConfiguration;
import br.com.m4u.recruitment.sms.SmsServiceApplication;
import br.com.m4u.recruitment.sms.model.Message;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:application-test.properties")
@ContextConfiguration(classes = {SmsServiceApplication.class, ExternalServiceConfiguration.class})
public class MessageServiceIT {
	
	private ClientAndServer mockServer;
	
	@Autowired private MessageService service;

	@Before
	public void setupWireMock(){
		mockServer = ClientAndServer.startClientAndServer(6767);
	}
	
	@After
	public void teardownWireMock(){
		mockServer.stop();
	}
	
	@Test
	public void should_send_sms() throws ExternalServiceException {
		
		mockServer.when(
				request()
					.withMethod("PUT")
					.withPath("/api/v1/sms")
					.withBody("{\"id\":123,\"from\":\"1234546\",\"to\":\"654321\",\"body\":\"Hello!\"}"),
				exactly(1)
		).respond(
				response()
					.withStatusCode(201)
		);
		
		Message dto = new Message();
		dto.setId(123L);
		dto.setFrom("1234546");
		dto.setTo("654321");
		dto.setBody("Hello!");
		
		service.send(dto);
	}
	
	@Test(expected = ExternalServiceException.class)
	public void shouldnt_send_sms_internal_server_error() throws ExternalServiceException {
		
		mockServer.when(
				request()
					.withMethod("PUT")
					.withPath("/api/v1/sms"),
				exactly(1)
		).respond(
				response()
					.withStatusCode(500)
		);
		
		Message dto = new Message();
		dto.setId(123L);
		dto.setFrom("1234546");
		dto.setTo("654321");
		dto.setBody("Hello!");
		service.send(dto);
	}
	
	@Test(expected = ExternalServiceException.class)
	public void shouldnt_send_sms_user_not_found_error() throws ExternalServiceException {
		
		mockServer.when(
				request()
					.withMethod("PUT")
					.withPath("/api/v1/sms"),
				exactly(1)
		).respond(
				response()
					.withStatusCode(404)
		);
		
		Message dto = new Message();
		dto.setId(123L);
		dto.setFrom("1234546");
		dto.setTo("654321");
		dto.setBody("Hello!");
		service.send(dto);
	}
	
	@Test(expected = ExternalServiceException.class)
	public void shouldnt_send_sms_user_validation_error() throws ExternalServiceException {
		
		mockServer.when(
				request()
					.withMethod("PUT")
					.withPath("/api/v1/sms"),
				exactly(1)
		).respond(
				response()
					.withStatusCode(400)
		);
		
		Message dto = new Message();
		dto.setId(123L);
		dto.setFrom("1234546");
		dto.setTo("654321");
		dto.setBody("Hello!");
		service.send(dto);
	}

}
