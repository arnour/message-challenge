package br.com.m4u.recruitment.sms.model;

import java.time.LocalDateTime;

import org.junit.Test;

import br.com.m4u.recruitment.sms.exception.ExpiredMessageException;

public class MessageTest {

	@Test
	public void null_expiration_date_should_be_valid() {
		new Message().checkExpiration();
	}
	
	public void future_expiration_date_should_be_valid() {
		Message message = new Message();
		message.setExpiresAt(LocalDateTime.now().plusDays(1L));
		message.checkExpiration();
	}
	
	@Test(expected = ExpiredMessageException.class)
	public void past_expiration_date_should_be_invalid() {
		Message message = new Message();
		message.setExpiresAt(LocalDateTime.now().minusDays(1L));
		message.checkExpiration();
	}

}
